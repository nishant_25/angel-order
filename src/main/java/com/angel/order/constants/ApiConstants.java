package com.angel.order.constants;

public class ApiConstants {

    public static final String ORDER = "/order";
    public static final String API = "/api";
    public static final String VERSION = "/v1";
    public static final String HEALTH_CHECK = "/health";
    public static final String PLACE = "/place";
}
