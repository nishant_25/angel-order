package com.angel.order.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class OrderModel {

    @JsonProperty("id")
    String id;

    @JsonProperty("product_id")
    String productId;

    @JsonProperty("user_id")
    String userId;

    @JsonProperty("status")
    String status;
}
