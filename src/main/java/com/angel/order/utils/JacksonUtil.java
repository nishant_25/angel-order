package com.angel.order.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
@RequiredArgsConstructor
@Slf4j
public class JacksonUtil {
    private final ObjectMapper objectMapper;

    public <T> T convertStringToObject(String input, Class<T> t) {
        try {
            return objectMapper.readValue(input, t);
        } catch (Exception e) {
            log.error("string input to object conversion failed ", e);
            throw new RuntimeException(e);
        }
    }

    public <T> T convertMapToObject(Map<String, Object> input, Class<T> t) {
        return objectMapper.convertValue(input, t);
    }

    public <T> List<T> convertStringToListObject(String input, Class<T> t) {
        try {
            return objectMapper.readValue(input, objectMapper.getTypeFactory().constructCollectionType(ArrayList.class, t));
        } catch (JsonProcessingException e) {
            log.error("string input to list object conversion failed ", e);
            throw new RuntimeException(e);
        }
    }

    public String convertObjectToString(Object obj) {
        try {
            return objectMapper.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    public Map<String, Object> convertObjectIntoMap(Object obj) {
        return objectMapper.convertValue(obj, Map.class);
    }
}
