package com.angel.order.controller;

import com.angel.order.constants.ApiConstants;
import com.angel.order.request.OrderRequest;
import com.angel.order.response.OrderResponse;
import com.angel.order.service.OrderService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(ApiConstants.ORDER)
@RequiredArgsConstructor
@Slf4j
public class OrderController {

    @Autowired
    OrderService orderService;

    @PostMapping(value = ApiConstants.API + ApiConstants.VERSION + ApiConstants.PLACE, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<OrderResponse> updateCart(@RequestBody OrderRequest orderRequest){
        return orderService.placeOrder(orderRequest);
    }
}
