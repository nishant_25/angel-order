package com.angel.order.db.repository;

import com.angel.order.db.entity.OrderEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

public interface OrderRepository extends JpaRepository<OrderEntity, String> {

    @Modifying
    @Transactional
    @Query(value="insert into transactions(id, product_id, user_id, status) values(?1, ?2, ?3, ?4)", nativeQuery = true)
    void placeOrder(String id, String productId, String userId, String status);

    @Modifying
    @Transactional
    @Query(value="update transactions set status=?1 where id=?2", nativeQuery = true)
    void updateOrder(String status, String id);

}
