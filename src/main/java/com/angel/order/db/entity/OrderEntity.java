package com.angel.order.db.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;

@Entity
@Table(name = "transactions")
@FieldDefaults(level = AccessLevel.PRIVATE)
@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class OrderEntity {

    @Id
    @Column(name = "id")
    @JsonProperty("id")
    String id;

    @Column(name = "product_id")
    @JsonProperty("product_id")
    String productId;

    @Column(name = "user_id")
    @JsonProperty("user_id")
    String userId;

    @Column(name = "status")
    @JsonProperty("status")
    String status;

    @Column(name = "created_at")
    @CreationTimestamp
    @JsonIgnore
    Timestamp createdAt;

    @Column(name = "updated_at")
    @UpdateTimestamp
    @JsonIgnore
    Timestamp updatedAt;
}
