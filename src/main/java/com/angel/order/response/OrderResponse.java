package com.angel.order.response;

import com.angel.order.db.entity.OrderEntity;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class OrderResponse{

    @JsonProperty("data")
    OrderEntity data;

    @JsonProperty("message")
    String message;

    @JsonProperty("error")
    String error;

    @JsonProperty("status")
    Integer statusCode;
}
