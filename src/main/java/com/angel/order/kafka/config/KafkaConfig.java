package com.angel.order.kafka.config;

import lombok.Data;
import lombok.experimental.FieldDefaults;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import static lombok.AccessLevel.PRIVATE;

@Configuration
@Data
@FieldDefaults(level = PRIVATE)
public class KafkaConfig {

    @Value("${spring.kafka.bootstrap-servers}")
    String bootstrapAddress;

    @Value("${order.confirm.group}")
    String digitalIngestGroupId;

    @Value("${order.confirm.topic}")
    String digitalIngestTopic;
}
