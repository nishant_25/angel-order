package com.angel.order.kafka.consumer;

import com.angel.order.client.InventoryClient;
import com.angel.order.db.repository.OrderRepository;
import com.angel.order.model.OrderModel;
import com.angel.order.utils.JacksonUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import java.util.Objects;


@Slf4j
@RequiredArgsConstructor
@Component
public class OrderConfirmationConsumer {

    @Autowired
    InventoryClient inventoryClient;

    @Autowired
    OrderRepository orderRepository;

    private final JacksonUtil jacksonUtil;

    @KafkaListener(topics = "${order.confirm.topic}", groupId = "${order.confirm.group}")
    public void orderConsumer(ConsumerRecord<String, String> consumerRecord) {
        try {
            if (consumerRecord.value() != null && !consumerRecord.value().isBlank()) {
                log.info("Received payload in Kafka Consumer {} : {}", consumerRecord.topic(), consumerRecord.value());
                String orderModelString = jacksonUtil.convertStringToObject(consumerRecord.value(), String.class);
                OrderModel orderModel = jacksonUtil.convertStringToObject(orderModelString, OrderModel.class);
                // do inventory check
                if (!Objects.isNull(inventoryClient.getInventory(orderModel.getProductId())) && inventoryClient.getInventory(orderModel.getProductId()).getInventoryModel().getAvailableInventory() > 0) {
                    // send notification to PG to process the payment and move the order to confirmed state and
                    // notification to user saying the order has been confirmed
                    orderRepository.updateOrder("CONFIRMED", orderModel.getId());
                    // call inventory service to reduce the inventory
                    inventoryClient.updateInventory(orderModel.getProductId());
                } else {
                    // send notification to PG to cancel the payment and send notification to customer saying order
                    // could not be confirmed and you have not been charged for the same
                }
            }

        } catch (Exception exception) {
            log.error("Unexpected exception while processing the order consumer record", exception);
        }

    }
}
