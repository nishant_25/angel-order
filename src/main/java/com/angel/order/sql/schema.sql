create database orderdb;

use orderdb;

CREATE TABLE `transactions` (
`id` varchar(100) NOT NULL,
`product_id` varchar(100) NOT NULL,
`user_id` varchar(100) NOT NULL,
`status` varchar(100) NOT NULL,
`created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
`updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;