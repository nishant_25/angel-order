package com.angel.order.service;

import com.angel.order.request.OrderRequest;
import com.angel.order.response.OrderResponse;
import org.springframework.http.ResponseEntity;

public interface OrderService {

    ResponseEntity<OrderResponse> placeOrder(OrderRequest orderRequest);
}
