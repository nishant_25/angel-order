package com.angel.order.service.impl;

import com.angel.order.client.InventoryClient;
import com.angel.order.db.entity.OrderEntity;
import com.angel.order.db.repository.OrderRepository;
import com.angel.order.kafka.config.KafkaConfig;
import com.angel.order.request.OrderRequest;
import com.angel.order.response.OrderResponse;
import com.angel.order.service.OrderService;
import com.angel.order.utils.JacksonUtil;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.UUID;

@Service
@RequiredArgsConstructor
@Slf4j
public class OrderServiceImpl implements OrderService {

    @Autowired
    OrderRepository orderRepository;

    @Autowired
    InventoryClient inventoryClient;

    @Autowired
    KafkaConfig kafkaConfig;

    @Autowired
    private KafkaTemplate<String, Object> kafkaTemplate;

    private final JacksonUtil jacksonUtil;

    private void sendOrderConfirmationEvent(OrderEntity orderEntity){
        try{
            ProducerRecord<String, Object> producerRecord = new ProducerRecord<>(kafkaConfig.getDigitalIngestTopic(), jacksonUtil.convertObjectToString(orderEntity));
            kafkaTemplate.send(producerRecord);
        }catch (Exception exception){
            log.error("Exception while sending the message to kafka : ", exception);
        }
    }

    @Override
    public ResponseEntity<OrderResponse> placeOrder(OrderRequest orderRequest){
        try{
            OrderResponse orderResponse = new OrderResponse();
            if (!Objects.isNull(inventoryClient.getInventory(orderRequest.getProductId())) && inventoryClient.getInventory(orderRequest.getProductId()).getInventoryModel().getAvailableInventory() > 0) {
                String id = UUID.randomUUID().toString();
                // Call payment gateway with user and payment details, once payment is verified, we'll place the order
                // Note: Here the money should not be deducted from the user. Money should be deducted only after the
                // order confirmation which will happen asynchronously
                orderRepository.placeOrder(id, orderRequest.getProductId(), orderRequest.getUserId(), "PLACED");
                OrderEntity orderEntity = new OrderEntity();
                orderEntity.setId(id);
                orderEntity.setStatus("PLACED");
                orderEntity.setProductId(orderRequest.getProductId());
                orderEntity.setUserId(orderRequest.getUserId());
                orderResponse.setData(orderEntity);
                orderResponse.setMessage("Order placed successfully");
                orderResponse.setStatusCode(200);
                // send message to kafka for order confirmation
                sendOrderConfirmationEvent(orderEntity);
                return new ResponseEntity<>(orderResponse, HttpStatus.OK);
            }else{
                orderResponse.setData(null);
                orderResponse.setError("Sorry this product is out of stock");
                orderResponse.setStatusCode(400);
                return new ResponseEntity<>(orderResponse, HttpStatus.BAD_REQUEST);
            }
        }catch(Exception exception){
            log.error("Unexpected exception while placing the order : ", exception);
            OrderResponse orderResponse = new OrderResponse();
            orderResponse.setData(null);
            orderResponse.setError("Unexpected error");
            orderResponse.setStatusCode(500);
            return new ResponseEntity<>(orderResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
