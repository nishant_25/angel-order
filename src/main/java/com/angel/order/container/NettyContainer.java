package com.angel.order.container;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.embedded.netty.NettyReactiveWebServerFactory;
import org.springframework.boot.web.embedded.netty.NettyServerCustomizer;
import org.springframework.boot.web.server.WebServer;
import org.springframework.context.annotation.Bean;
import org.springframework.http.server.reactive.ContextPathCompositeHandler;
import org.springframework.http.server.reactive.HttpHandler;
import org.springframework.stereotype.Component;
import org.zalando.logbook.Logbook;
import org.zalando.logbook.netty.LogbookServerHandler;
import reactor.netty.http.server.HttpServer;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@RequiredArgsConstructor
@Component
public class NettyContainer {
    @Value("${server.servlet.context-path}")
    String contextPath;

    private final Logbook logbook;

    @Bean
    public NettyReactiveWebServerFactory undertowReactiveWebServerFactory() {
        NettyReactiveWebServerFactory webServerFactory =
                new NettyReactiveWebServerFactory() {
                    @Override
                    public WebServer getWebServer(HttpHandler httpHandler) {
                        Map<String, HttpHandler> handlerMap = new HashMap<>();
                        handlerMap.put(contextPath, httpHandler);
                        return super.getWebServer(new ContextPathCompositeHandler(handlerMap));
                    }
                };

        webServerFactory.addServerCustomizers(netty());
        webServerFactory.addServerCustomizers(portCustomizer());
        return webServerFactory;
    }

    public NettyServerCustomizer netty() {
        return httpServer ->
                HttpServer.create()
                        .tcpConfiguration(
                                tcpServer ->
                                        tcpServer.doOnConnection(
                                                connection ->
                                                        connection.addHandlerLast(new LogbookServerHandler(logbook))));
    }

    public NettyServerCustomizer portCustomizer() {
        return httpServer -> httpServer.port(8081);
    }
}

