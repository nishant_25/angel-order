package com.angel.order.container;

import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.binder.okhttp3.OkHttpMetricsEventListener;
import io.opentracing.Tracer;
import io.opentracing.contrib.okhttp3.TracingInterceptor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import okhttp3.logging.LoggingEventListener;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

@Component
@Slf4j
@RequiredArgsConstructor
public class ClientContainer {
    private final Tracer tracer;
    private final MeterRegistry registry;

    @Bean
    public OkHttpClient client() {
//        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();

        var httpClient = new OkHttpClient.Builder()
//                .eventListener(OkHttpMetricsEventListener.builder(registry, "okhttp.requests")
//                        .uriMapper(req -> req.url().encodedPath())
//                        .build())
//                .eventListenerFactory(new LoggingEventListener.Factory())
//                .addNetworkInterceptor(logging)
                .connectTimeout(10000, TimeUnit.MILLISECONDS)
                .readTimeout(10000, TimeUnit.MILLISECONDS);

        return TracingInterceptor.addTracing(httpClient, tracer);
    }
}