package com.angel.order.container;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebFilter;
import org.springframework.web.server.WebFilterChain;
import reactor.core.publisher.Mono;
import reactor.util.context.Context;

@Component
@Slf4j
public class MDCFilter implements WebFilter {

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, WebFilterChain chain) {
        HttpHeaders headers = exchange.getRequest().getHeaders();
        return chain
                .filter(exchange)
                .doAfterSuccessOrError((r, t) -> logWithContext(headers))
                .subscriberContext(Context.of(HttpHeaders.class, headers));
    }

    private void logWithContext(HttpHeaders headers) {
        headers.forEach(
                (name, value) -> {
                    if (name.equalsIgnoreCase("x-request-id")) {
                        MDC.put("request_id", value.get(0));
                    }
                });
    }
}
